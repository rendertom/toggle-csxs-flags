![Toggle CSXS Flags](/Toggle%20CSXS%20Flags.png)

# Toggle CSXS Flags #
Utility tool for After Effects and Photoshop to toggle **Adobe CEP CSXS flags**.

```Curently only works on Mac```

### Installation: ###
Clone or download this repository.

**After Effects** - place **Toggle CSXS Flags.jsx** to ScriptUI Panels folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking Window -> **Toggle CSXS Flags**

**Photoshop** - place **Convert to Binary.jsx** script to Photoshop’s Scripts folder:
```Adobe Photoshop CC 20XX -> Presets -> Scripts -> Clean JS.jsx```

Restart Photoshop to access script from File -> Scripts -> **Toggle CSXS Flags**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------