/**********************************************************************************************
    Toggle CSXS Flags.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
    	Utility tool for After Effects and Photoshop to toggle Adobe CEP CSXS flags.
	
	TODO:
		Make it work on Windows
**********************************************************************************************/

(function (thisObj) {
	var CSXSData = function () {
		var isWin = $.os.indexOf("Win") !== -1;
		var isAE = BridgeTalk.appName === "aftereffects";
		var isPS = BridgeTalk.appName === "photoshop";

		function getData() {
			var regFolder = getRegFolder(),
				regFiles = getRegFiles(regFolder),
				data = buildData(regFiles);

			return data;
		}

		function getRegFolder() {
			var regFolder;

			if (isWin) {

			} else {
				regFolder = Folder(Folder.userData.parent.fsName + "/" + "Preferences");
			}

			return regFolder;
		}

		function getRegFiles(regFolder) {
			var regFiles,
				searchMask = "";

			if (isWin) {

			} else {
				searchMask = "com.adobe.CSXS*";
				regFiles = regFolder.getFiles(searchMask);
			}

			return regFiles;
		}

		function buildData(regFiles) {
			var dataArray = [],
				csxsData = {},
				version,
				value;

			for (var i = 0, il = regFiles.length; i < il; i++) {
				version = getCsxsVersion(regFiles[i]);
				value = parseInt(readFlag(version));
				csxsData = {
					version: version,
					value: value,
					file: regFiles[i].fsName
				};

				dataArray.push(csxsData);
			}
			return dataArray;
		}

		function getCsxsVersion(regFile) {
			var csxsVersion;

			if (isWin) {

			} else {
				csxsVersion = regFile.displayName.split(".")[3];
			}

			return csxsVersion;
		}

		function readFlag(csxsVersion) {
			var cmd,
				response;

			if (isWin) {
				cmd = "REG Query HKCU\\Software\\Adobe\\CSXS." + csxsVersion + " /v PlayerDebugMode";
			} else {
				cmd = "defaults read com.adobe.CSXS." + csxsVersion + " PlayerDebugMode";
			}

			response = systemCall(cmd);

			return response;
		}

		function writeFlag(csxsVersion, value) {
			var cmd;

			if (isWin) {
				cmd = "REG ADD HKCU\\Software\\Adobe\\CSXS." + csxsVersion + " /f /v PlayerDebugMode /t REG_SZ /d " + value;
			} else {
				cmd = "defaults write com.adobe.CSXS." + csxsVersion + " PlayerDebugMode " + value;
			}
			systemCall(cmd);
		}

		function systemCall(cmd) {
			var response = "";
			if (isAE) {
				response = system.callSystem(cmd);
			} else if (isPS) {
				var tempOutputFile = (isWin ? Folder.temp.fsName : Folder.temp.absoluteURI) + "/" + Math.round(Math.random() * (new Date()).getTime() * 21876);

				app.system(cmd + " > " + tempOutputFile);
				response = readFileContent(tempOutputFile);
				File(tempOutputFile).remove();
			}
			return response;
		}

		function readFileContent(fileObj, encoding) {
			var fileContent;

			encoding = encoding || "utf-8";
			fileObj = (fileObj instanceof File) ? fileObj : new File(fileObj);

			fileObj.open('r');
			fileObj.encoding = encoding;
			fileContent = fileObj.read();
			fileObj.close();
			return fileContent;
		}

		return {
			getData: getData,
			setFlag: writeFlag,
			add: writeFlag
		};
	};

	buildUI(thisObj);

	function buildUI(thisObj) {
		var btnSize = 24,
			CSXS = CSXSData(),
			minSpacing = 2,
			typeOfWindow = getTypeOfWindow(),
			win;			

		win = (thisObj instanceof Panel) ? thisObj : new Window(typeOfWindow, "CSXS Flags", undefined);
		win.alignChildren = ["fill", "fill"];
		win.spacing = minSpacing;

		win.list = win.add("listbox", undefined, "");

		win.list.preferredSize.height = 120;
		win.list.onChange = function () {
			win.btnToggle.enabled = true;
			win.btnRemove.enabled = true;
			if (!this.selection) {
				win.btnToggle.enabled = false;
				win.btnRemove.enabled = false;
			}
		};

		win.grpButtons = win.add("group");
		win.grpButtons.alignment = ["fill", "bottom"];
		win.grpButtons.spacing = minSpacing;
		win.btnAdd = win.grpButtons.add("button", undefined, "+");
		win.btnAdd.preferredSize = [btnSize, btnSize];
		win.btnAdd.helpTip = "Create new CSXS preference properties file.";
		win.btnAdd.onClick = function () {
			var latestVersion = getLatestCsxsVersion() + 1;
			var newCsxsVersion = prompt("Create new CSXS preference properties file\nFlag \"PlayerDebugMode\" will be set to 1.", latestVersion);
			if (newCsxsVersion) {
				if (isNaN(newCsxsVersion)) {
					alert("Please specify positive CSXS numeric value.");
					win.btnAdd.onClick();
				} else if (csxsExists(newCsxsVersion)) {
					alert("CSXS " + newCsxsVersion + " preference properties file already exists.");
					win.btnAdd.onClick();
				} else {
					CSXS.add(newCsxsVersion, 1);
					updateList(win.list);
				}
			}
		};
		win.btnRemove = win.grpButtons.add("button", undefined, "-");
		win.btnRemove.preferredSize = [btnSize, btnSize];
		win.btnRemove.helpTip = "Remove selected CSXS preference properties file.";
		win.btnRemove.onClick = function () {
			if (confirm("Are you sure you want to remove CSXS " + win.list.selection.version + " preference properties file?")) {
				File(win.list.selection.file).remove();
				updateList(win.list);
			}
		};
		win.btnRefresh = win.grpButtons.add("button", undefined, "R");
		win.btnRefresh.preferredSize = [btnSize, btnSize];
		win.btnRefresh.helpTip = "Refresh the list.";
		win.btnRefresh.onClick = function () {
			updateList(win.list);
		};
		win.btnToggle = win.grpButtons.add("button", undefined, "Toggle");
		win.btnToggle.preferredSize.height = btnSize;
		win.btnToggle.helpTip = "Set / unset \"PlayerDebugMode\" flag.";
		win.btnToggle.onClick = function () {
			var csxsVersion = win.list.selection.version,
				flagValue = win.list.selection.flagValue,
				newFlagValue = toggleFlagValue(flagValue);

			CSXS.setFlag(csxsVersion, newFlagValue);

			updateList(win.list);
		};


		win.onShow = function () {
			updateList(win.list);
		};

		win.onResizing = win.onResize = function () {
			this.layout.resize();
		};

		if (win instanceof Window) {
			win.center();
			win.show();
		} else {
			win.layout.layout(true);
			win.layout.resize();
		}

		function updateList(listbox) {

			var csxsData = CSXS.getData(),
				item,
				selectionIndex;

			if (listbox.selection)
				selectionIndex = listbox.selection.index;

			listbox.removeAll();

			for (var i = 0, il = csxsData.length; i < il; i++) {
				item = listbox.add("item");
				item.file = csxsData[i].file;
				item.flagValue = csxsData[i].value;
				item.version = csxsData[i].version;

				item.text = "Adobe CSXS " + item.version + " - " + item.flagValue;
			}

			if (typeof selectionIndex !== "undefined") {
				if (selectionIndex > listbox.items.length - 1) {
					selectionIndex = listbox.items.length - 1;
				}
				listbox.selection = selectionIndex;
			}

			listbox.onChange();
		}

		function toggleFlagValue(flagValue) {
			var newFlagValue = 1;

			if (parseInt(flagValue) === 1)
				newFlagValue = 0;

			return newFlagValue;
		}

		function csxsExists(csxsVersion) {
			var csxsData = CSXS.getData();
			for (var i = 0, il = csxsData.length; i < il; i++) {
				if (csxsData[i].version === csxsVersion) {
					return true;
				}
			}
			return false;
		}

		function getLatestCsxsVersion() {
			var csxsData = CSXS.getData(),
				latestVersion = 0;

			for (var i = 0, il = csxsData.length; i < il; i++) {
				if (latestVersion < parseInt(csxsData[i].version)) {
					latestVersion = parseInt(csxsData[i].version);
				}
			}
			return latestVersion;
		}

		function getTypeOfWindow() {
			var isAfterEffects = function () {
				return BridgeTalk.appName.toLowerCase().match("aftereffects");
			};

			return isAfterEffects() ? "palette" : "dialog";
		}
	}

})(this);